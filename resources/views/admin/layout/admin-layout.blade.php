<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Panel</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{ asset('assets/jquery-ui/themes/base/minified/jquery-ui.min.css')}}" rel="stylesheet" />
    <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/assets/css/style.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap-datepicker3.min.css" />
    <!-- ================== END BASE CSS STYLE ================== -->
    <script src="/assets/js/pace.min.js"></script>
    <script src="/assets/jquery/jquery-1.9.1.min.js"></script>
    <script src="/assets/jquery/jquery-ui.min.js"></script>
    <script src="/assets/js/apps.min.js"></script>
    <script src="/assets/jquery/jquery.slimscroll.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap-datepicker.min.js"></script>
    <script src="/assets/bootstrap/js/ckeditor.js"></script>
    {{--<script src="/assets/bootstrap-datepicker/moment.js" />--}}
    {{--<script src="/assets/bootstrap-datepicker/bootstrap-datetimepicker.min.js" />--}}

</head>
<body>
<!-- begin #header -->
<div id="header" class="header navbar navbar-default" >
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="/admin" class="navbar-brand"><span class="navbar-logo"></span> Admin panel</a>
            <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <!-- begin header navigation right -->
        @if(Auth::check())
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown navbar-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu animated fadeInLeft">
                        <li class="arrow"></li>
                        {{--<li><a href="javascript:;">Edit Profile</a></li>--}}
                        <li><a href="/auth/logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        @endif
        <!-- end header navigation right -->
    </div>
    <!-- end container-fluid -->
</div>
@if(Auth::check())

<div id="sidebar" class="sidebar" style="position: fixed">
    <!-- begin sidebar scrollbar -->
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div data-scrollbar="true" data-height="100%" data-init="true" style="overflow: hidden; width: auto; height: 100%;">

            <!-- begin sidebar nav -->
            <ul class="nav">
                <li class="nav-header">Navigation</li>
                <li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        <i class="fa fa-laptop"></i>
                        <span>Dashboard</span>
                    </a>
                    <ul class="sub-menu" style="display: block;">
                        <li><a href="/">Admin</a></li>

                    </ul>
                </li>

            </ul>
            <!-- end sidebar nav -->
        </div>
    <!-- end sidebar scrollbar -->
</div>
</div>
@endif
<div class="row">
    <!-- begin #content -->
    <div class="container-fluid m-t-10">
        @yield('content')
    </div>
    <!-- end #content -->
</div>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
</body>
</html>