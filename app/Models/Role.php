<?php
/**
 * User: Michael Lazarev <mihailaz.90@gmail.com>
 * Date: 03.11.16
 * Time: 9:16
 */


namespace App\Models;

use Zizaco\Entrust\EntrustRole;


/**
 * Class Role
 * @package App
 */
class Role extends EntrustRole
{
	const ADMIN = 'Admin';
	const MODER = 'Manager';

	public static function add($role)
	{
		$Role = new Role();
		$Role->name = $role;
		$Role->save();
		return $Role;
	}

	public static function getRole($role){
		return Role::where('name',$role)->first();
	}
}