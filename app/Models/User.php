<?php
namespace App\Models;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends \Eloquent
{
	use EntrustUserTrait; // add this trait to your user model

	protected $fillable = [
		'first_name', 'last_name', 'email', 'password', 'blocked', 'country_id', 'role'
	];

	public function Validate(){

		$rules = array(
			'last_name' => array('required','max:255'),
			'first_name'=> array('required','max:255'),
			'email'     => array('required','email','max:255','unique:users'),
			'country_id'=> array('required','int'),
		);

		$model = self::getModel();
		$email = $model->getOriginal('email');

		$model = $model->toArray();
		if($email == $model['email']) unset($rules['email']);

		$validator = \Validator::make($model, $rules);
		if ($validator->fails()) {
			return $validator;
		}
		return true;
	}


	public static function addRole(User $user, $role){

		$user->attachRole($role);
	}
	
	public static function registration($input){
		$user =  new User([
			'last_name'   => $input['last_name'],
			'first_name'  => $input['first_name'],
			'email'       => $input['email'],
			'blocked'     => isset($input['blocked']) ? true : false,
			'password'    => bcrypt($input['password']),
			'country_id'  => $input['country_id'],
			'role'        => $input['role'],
		]);

		if($user->Validate() === true){
			$user->save();
			$user->attachRole(Role::where('id', $input['role'])->first());
			return true;

		}
		return $user->Validate();
	}

	public static function edit($input, $id){

		$user = User::where('id', $id)->first();
		$user->last_name  = $input['last_name'];
		$user->first_name = $input['first_name'];
		$user->email      = $input['email'];
		$user->blocked    = isset($input['blocked']) ? true : false;
		if($input['password']) $user->password   = bcrypt($input['password']);
		$user->country_id = $input['country_id'];
		$user->role       = $input['role'];
		if($user->Validate() === true){
			$user->detachRoles();
			$user->attachRole(Role::where('id', $input['role'])->first());

			return $user->save();
		}
		return $user->Validate();

	}

	public function country(){
		return $this->belongsTo('App\Models\Admin\Country', 'country_id','id')
			->select(['id', 'city']);
	}
}