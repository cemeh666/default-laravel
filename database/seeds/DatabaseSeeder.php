<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'admin',
            'email'      => 'admin@admin.admin',
            'role'       => 1,
            'password'   => bcrypt('admin@admin.admin'),
        ]);

        \App\Models\Role::add(\App\Models\Role::ADMIN);
        \App\Models\Role::add(\App\Models\Role::MODER);

        $user = \App\Models\User::where('id', 1)->first();
        $user->attachRole(\App\Models\Role::where('name', \App\Models\Role::ADMIN)->first());
    }
}
